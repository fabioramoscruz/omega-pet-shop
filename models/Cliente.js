const mongoose = require('mongoose');

const productoSchema = mongoose.Schema({
    nombre:{
        type: String,
        required: true
    },
    apellido:{
        type: String,
        required: true
    },
    correo:{
        type: String,
        required: true
    },
    cantidad:{
        type: Number,
        required: true
    },
    precio_venta:{
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Producto', productoSchema );